//
//  ProductSearchViewController.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "ProductSearchViewController.h"
#import "ProductSearchDataManager.h"
#import "Search.h"
#import "EmptyTableViewCell.h"
#import "ProductTableViewCell.h"
#import "Product.h"
#import "UIImageView+LoadImage.h"
#import "UIColor+BestSearch.h"
#import "SearchCriteria.h"
#import "BestBuySearchSDK.h"
#import "ProductDetailViewController.h"
#import "ProductDetailCollectionViewController.h"
#import "PersistentDataManager.h"
#import "NotationView.h"

#define VIEW_CONTROLLER_PRODUCT_DETAIL @"ProductDetailViewController"
#define VIEW_CONTROLLER_PRODUCT_DETAIL_COLLECTION_VIEW @"ProductDetailCollectionViewController"

#define EMPTY_TABLE_CELL @"EmptyTableViewCell"
#define PRODUCT_TABLE_CELL @"ProductTableViewCell"

@interface ProductSearchViewController ()<UITableViewDataSource, UITableViewDelegate, EmptyTableViewCellDelegate, UITextFieldDelegate, ProductDetailCollectionViewControllerDelegate> {
    BOOL onRefresh;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
- (IBAction)onSearchClicked:(id)sender;
@property (nonatomic, strong) Search *search;
@property (nonatomic, strong) NSURL *baseUrl;
@property (nonatomic, assign) NSInteger emptyCellCount;
@property (nonatomic, strong) NSIndexPath *indexPath;

@end

@implementation ProductSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.search = [[Search alloc] init];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    self.baseUrl = [NSURL URLWithString:[BestBuySearchSDK instance].imageBaseUrl];
    
    //Search button treatment
    self.searchButton.layer.cornerRadius = 15.0f;
    self.searchButton.layer.backgroundColor = [UIColor colorWithHexString:@"3998EF"].CGColor;
    
    self.searchTextField.layer.cornerRadius = 15.0f;
    self.searchTextField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)refresh {
    
    onRefresh = YES;
    
    [[ProductSearchDataManager instance].search reset];
    
    [self.tableView reloadData];
    
}

- (void)executeSearch {
    [ProductSearchDataManager instance].search.criteria.query = self.searchTextField.text;
    [self refresh];
}

- (IBAction)onSearchClicked:(id)sender {
    [self executeSearch];
    
    if([self.searchTextField isFirstResponder]){
        [self.searchTextField resignFirstResponder];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)emptyCellCount {
    return [ProductSearchDataManager instance].search.moreResult? 1 : 0;
}

#pragma mark - UITableViewDelegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [ProductSearchDataManager instance].search.products.count + self.emptyCellCount;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *uniqueIdentifier = (indexPath.row >= [ProductSearchDataManager instance].search.products.count) ? EMPTY_TABLE_CELL : PRODUCT_TABLE_CELL;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:uniqueIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }

    if ([uniqueIdentifier isEqualToString:EMPTY_TABLE_CELL]) {
        
        EmptyTableViewCell* emptyCell = (EmptyTableViewCell*)cell;
        emptyCell.delegate = self;
        
    }
    else if ([uniqueIdentifier isEqualToString:PRODUCT_TABLE_CELL]) {
        
        ProductTableViewCell *productCell = (ProductTableViewCell *)cell;
        NSArray *products = [ProductSearchDataManager instance].search.products;
        
        if (indexPath.row < products.count) {
            Product *product = (Product *)products[indexPath.row];
            productCell.productTitle.text = product.name;
            
            productCell.featureView.backgroundColor = product.salePrice < product.regularPrice? [UIColor redColor] : [UIColor clearColor];
            
            if ([[PersistentDataManager instance] getProductBySku:product.sku]) {
                productCell.featureView.notationType = NOTATION_TYPE_FOLLOW_ME;
//                [productCell.imageView followMe];
//                productCell.productView.backgroundColor = [UIColor lightGrayColor];
            }
            else {
                productCell.featureView.notationType = NOTATION_TYPE_NONE;
//                productCell.productView.backgroundColor = [UIColor colorWithHexString:@"F4F4F4"];
            }
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
            productCell.productPrice.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:product.salePrice]];
            //image
            NSURL *imageUrl = [NSURL URLWithString:product.thumbnailImage relativeToURL:self.baseUrl];
            [productCell.productImageView loadImageFromURL:imageUrl withPlaceholder:NO_IMAGE_NAME withCompletionBlock:^(UIImage *image) {
                //retrieve cell again to make sure the cell in context still visible
                ProductTableViewCell *cell = (ProductTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                cell.productImageView.image = image;
            }];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    Product *product = indexPath.row < [ProductSearchDataManager instance].search.products.count ? [ProductSearchDataManager instance].search.products[indexPath.row] : nil;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:STORYBOARD_PRODUCT_SEARCH bundle:[NSBundle mainBundle]];
    
//    ProductDetailViewController *productDetailViewController = (ProductDetailViewController*)[storyboard instantiateViewControllerWithIdentifier:VIEW_CONTROLLER_PRODUCT_DETAIL];
//    productDetailViewController.product = product;
    
    self.indexPath = indexPath;
    
    ProductDetailCollectionViewController *productionDetailCollectionViewController = (ProductDetailCollectionViewController*)[storyboard instantiateViewControllerWithIdentifier:VIEW_CONTROLLER_PRODUCT_DETAIL_COLLECTION_VIEW];
    productionDetailCollectionViewController.delegate = self;

    [self presentViewController:productionDetailCollectionViewController animated:YES completion:nil];
}

#pragma mark - EmptyTableViewCellDelegate method
- (void)searchComplete:(BOOL)success {
    
    if (success) {
        [[ProductSearchDataManager instance].search incrementPage];
        [self.tableView reloadData];
    }
    else {
        [Utilities showStandardAlertViewWithMessage:[@"oops" localizedString] fromViewController:self];
    }

    if (onRefresh) {
        onRefresh = NO;

        // End the refreshing
        if (self.refreshControl) {
            
            [self.refreshControl endRefreshing];
        }
        
    }
}

#pragma mark - ProductDetailCollectionViewControllerDelegate method
- (Product *)updateProductForCell:(NSIndexPath *)indexPath {
    NSInteger index = indexPath.row;
    return indexPath.row < [ProductSearchDataManager instance].search.products.count ? [ProductSearchDataManager instance].search.products[index] : nil;

}

- (NSInteger)totalProducts {
    return [ProductSearchDataManager instance].search.products.count;
}

- (NSIndexPath*)getIndexPath {
    return self.indexPath;
}

- (void)exitProductDetail {
    [self.tableView reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self executeSearch];
    [textField resignFirstResponder];
    return NO;
}

@end
