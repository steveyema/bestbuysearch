//
//  ProductSearchDataManager.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Search;

@interface ProductSearchDataManager : NSObject

@property (nonatomic, readonly) Search *search;

+ (ProductSearchDataManager*)instance;

/**
 *  Execute search
 *
 *  @param completion return YES for successfully complete the search
 */
- (void)executeSearchWithCompletionBlock:(void(^)(BOOL))completion;
/**
 *  Get product detail by sku ID
 *
 *  @param sku        unique sku ID
 *  @param completion completion block return Product object
 */
- (void)productDetailBySku:(NSString *)sku withCompletionBloack:(void(^)(id))completion;

@end
