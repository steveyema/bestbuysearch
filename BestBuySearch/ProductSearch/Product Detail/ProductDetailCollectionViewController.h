//
//  ProductDetailCollectionViewController.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-13.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Product;

@protocol ProductDetailCollectionViewControllerDelegate <NSObject>

- (NSInteger)totalProducts;
- (Product*)updateProductForCell:(NSIndexPath *)indexPath;
- (NSIndexPath*)getIndexPath;
- (void)exitProductDetail;

@end

@interface ProductDetailCollectionViewController : UIViewController


@property (nonatomic, weak) id<ProductDetailCollectionViewControllerDelegate> delegate;
@property (nonatomic, strong) Product *product;

@end
