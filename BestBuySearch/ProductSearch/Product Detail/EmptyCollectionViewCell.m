//
//  EmptyCollectionViewCell.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-15.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "EmptyCollectionViewCell.h"
#import "ProductSearchDataManager.h"

@implementation EmptyCollectionViewCell

- (void)layoutSubviews {
    [super layoutSubviews];
    [[ProductSearchDataManager instance] executeSearchWithCompletionBlock:^(BOOL successful) {
        //
        if (self.delegate && [self.delegate respondsToSelector:@selector(searchComplete:)]) {
            [self.delegate searchComplete:successful];
        }
    }];
}

@end
