//
//  EmptyCollectionViewCell.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-15.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EmptyCollectionViewCellDelegate <NSObject>

-(void)searchComplete:(BOOL)success;

@end

@interface EmptyCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id<EmptyCollectionViewCellDelegate> delegate;

@end
