//
//  ProduceDetailCollectionViewCell.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-14.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "ProduceDetailCollectionViewCell.h"
#import "ProductDetailTableViewCell.h"

#define DETAIL_CELL_UNIQUEIDENTIFIER @"ProductDetailTableViewCell"

@interface ProduceDetailCollectionViewCell()<UITableViewDataSource, UITableViewDelegate,  ProductDetailTableViewCellDelegate>{}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ProduceDetailCollectionViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

//- (void)setProduct:(Product *)product {
//    _product = product;
//    [self.tableView reloadData];
//}
//
#pragma mark - UITableViewDelegate method
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  [Utilities calculateSizeBaseOnScreenSize:1200.0f];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *uniqueIdentifier = DETAIL_CELL_UNIQUEIDENTIFIER;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:uniqueIdentifier];
    
    ProductDetailTableViewCell *productDetailCell = (ProductDetailTableViewCell *)cell;
    if (productDetailCell) {
        productDetailCell.dataSource = self.dataSource;
        productDetailCell.delegate = self;
    }
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:uniqueIdentifier];
    }
    
    return cell;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.dataSource respondsToSelector:@selector(scrollViewWillBeginDragging)]) {
        [self.dataSource scrollViewWillBeginDragging];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([self.dataSource respondsToSelector:@selector(scrollViewDidEndDecelerating)]) {
        [self.dataSource scrollViewDidEndDecelerating];
    }
}

#pragma mark - ProductDetailTableViewCellDelegate method
- (void)refresh {
    [self.tableView reloadData];
}

@end
