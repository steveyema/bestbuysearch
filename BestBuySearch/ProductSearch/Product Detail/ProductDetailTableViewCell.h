//
//  ProductDetailTableViewCell.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-12.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Product;

@protocol ProductDetailTableViewCellDataSource <NSObject>

- (Product*)getData;
- (void)scrollViewWillBeginDragging;
- (void)scrollViewDidEndDecelerating;
//obsolete
//- (void)onAddToCart;
//- (void)onDimiss;

@end

@protocol ProductDetailTableViewCellDelegate <NSObject>

//- (void)onAddToCart;
//- (void)onDimiss;

- (void)refresh;

@end

@interface ProductDetailTableViewCell : UITableViewCell

@property (nonatomic, weak) id<ProductDetailTableViewCellDataSource> dataSource;
@property (nonatomic, weak) id<ProductDetailTableViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *salePriceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;

@end
