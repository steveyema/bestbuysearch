//
//  ProductDetailViewController.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-12.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "ProductDetailTableViewCell.h"

#define DETAIL_CELL_UNIQUEIDENTIFIER @"ProductDetailTableViewCell"

@interface ProductDetailViewController()<UITableViewDataSource, UITableViewDelegate, ProductDetailTableViewCellDelegate>{}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

#pragma mark - UITableViewDelegate method
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  [Utilities calculateSizeBaseOnScreenSize:1200.0f];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *uniqueIdentifier = DETAIL_CELL_UNIQUEIDENTIFIER;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:uniqueIdentifier];
    
    ProductDetailTableViewCell *productDetailCell = (ProductDetailTableViewCell *)cell;
    if (productDetailCell) {
        productDetailCell.delegate = self;
    }
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:uniqueIdentifier];
    }
    
    return cell;
}

#pragma mark - ProductDetailTableViewCellDelegate method
- (void)refresh {
    [self.tableView reloadData];
}

- (Product*)getData {
    return self.product;
}

- (void)onAddToCart {
    NSLog(@"onAddToCartClicked");
    
    //TODO add to cart
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)onDimiss {
    NSLog(@"onAddToCartClicked");
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
