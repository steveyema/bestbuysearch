//
//  ProductDetailTableViewCell.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-12.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "ProductDetailTableViewCell.h"
#import "ProductDetail.h"
#import "ProductSearchDataManager.h"
#import "UIImageView+LoadImage.h"
#import "BestBuySearchSDK.h"

@interface ProductDetailTableViewCell(){}

@property (nonatomic, strong) Product *product;
@property (nonatomic, strong) ProductDetail *detail;

//@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
//@property (weak, nonatomic) IBOutlet UIButton *addToCartButton;
//- (IBAction)onAddToCartClicked:(id)sender;
//- (IBAction)onCancelClicked:(id)sender;

@end

@implementation ProductDetailTableViewCell

- (void)awakeFromNib {
    // Initialization code
//    self.cancelButton.layer.cornerRadius = 5.0f;
//    self.addToCartButton.layer.cornerRadius = 5.0f;
    self.descriptionLabel.layer.cornerRadius = 5.0f;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(getData)]) {
         Product *product = [[self.dataSource getData] copy];
        if (![product.sku isEqualToString:self.product.sku]) {
            self.detail = nil;
        }
        self.product = product;
    }
    
    if (!self.detail) {
        
        [self renderSummary];
        
        __weak ProductDetailTableViewCell *weakself = self;
        [[ProductSearchDataManager instance] productDetailBySku:self.product.sku withCompletionBloack:^(id obj) {
            
            ProductDetail *productDetail = (ProductDetail *)obj;
            if (productDetail) {
                weakself.detail = productDetail;
                if (self.delegate && [self.delegate respondsToSelector:@selector(refresh)]) {
                    [self.delegate refresh];
                }
            }
        }];
    }
    else {
        [self renderDetail];
    }
}

- (void)renderSummary {
    self.productTitleLabel.text = self.product.name;
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    self.priceLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:self.product.regularPrice]];
    self.salePriceLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:self.product.salePrice]];
    self.descriptionLabel.text = self.product.shortDescription;

    NSURL *imageUrl = [NSURL URLWithString:self.product.thumbnailImage relativeToURL:[NSURL URLWithString:[BestBuySearchSDK instance].imageBaseUrl]];
    __weak ProductDetailTableViewCell *weakself = self;
    [self.productImageView loadImageFromURL:imageUrl withPlaceholder:NO_IMAGE_NAME withCompletionBlock:^(UIImage *image) {
        weakself.productImageView.image = image;
    }];
}

- (void)renderDetail {
    //try get the high resolution image if possible
    NSString *hrImageUrlString = [self.detail.thumbnailImage stringByReplacingOccurrencesOfString:@"55x55" withString:@"500x500"];
    if (![hrImageUrlString isEqualToString:self.detail.thumbnailImage]) {
        NSURL *hrImageUrl = [NSURL URLWithString:hrImageUrlString relativeToURL:[NSURL URLWithString:[BestBuySearchSDK instance].imageBaseUrl]];
        __weak ProductDetailTableViewCell *weakself = self;
        [self.productImageView loadImageFromURL:hrImageUrl withPlaceholder:nil withCompletionBlock:^(UIImage *image) {
            weakself.productImageView.image = image;
        }];
    }
    //render more detail base on UI design
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (IBAction)onAddToCartClicked:(id)sender {
//    
//    if (self.delegate && [self.delegate respondsToSelector:@selector(onAddToCart)]) {
//        [self.delegate onAddToCart];
//    }
//}
//
//- (IBAction)onCancelClicked:(id)sender {
//    if (self.delegate && [self.delegate respondsToSelector:@selector(onDimiss)]) {
//        [self.delegate onDimiss];
//    }
//}
@end
