//
//  ProductDetailCollectionViewController.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-13.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "ProductDetailCollectionViewController.h"
#import "ProduceDetailCollectionViewCell.h"
#import "ProductDetailTableViewCell.h"
#import "EmptyCollectionViewCell.h"
#import "ProductSearchDataManager.h"
#import "Search.h"
#import "PersistentDataManager.h"

#define EMPTY_TABLE_CELL @"EmptyCollectionViewCell"
#define PRODUCT_TABLE_CELL @"ProduceDetailCollectionViewCell"

@interface ProductDetailCollectionViewController()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, ProductDetailTableViewCellDataSource, EmptyCollectionViewCellDelegate>{
    BOOL slideAway;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *addToCartButton;
- (IBAction)onAddToCart:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *onCancel;
- (IBAction)onCancel:(id)sender;

@end

@implementation ProductDetailCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cancelButton.layer.cornerRadius = 5.0f;
    self.addToCartButton.layer.cornerRadius = 5.0f;

    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.collectionView setPagingEnabled:YES];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

        NSIndexPath *scrollIndexPath = [self.delegate getIndexPath];
    
        [self.collectionView scrollToItemAtIndexPath:scrollIndexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    NSIndexPath *scrollIndexPath = [self.delegate getIndexPath];

//    [self.view layoutIfNeeded];
//    [self.collectionView scrollToItemAtIndexPath:scrollIndexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
}

- (NSInteger)emptyCellCount {
    return [ProductSearchDataManager instance].search.moreResult? 1 : 0;
}

#pragma mark - UICollectionViewDelegate method
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([self.delegate respondsToSelector:@selector(totalProducts)]) {
        return [self.delegate totalProducts] + self.emptyCellCount;
    }

    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return collectionView.bounds.size;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.collectionView.collectionViewLayout invalidateLayout];
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *uniqueIdentifier = (indexPath.row >= [ProductSearchDataManager instance].search.products.count) ? EMPTY_TABLE_CELL : PRODUCT_TABLE_CELL;

    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:uniqueIdentifier forIndexPath:indexPath];
    
    
    if ([uniqueIdentifier isEqualToString:EMPTY_TABLE_CELL]) {
        EmptyCollectionViewCell* emptyCell = (EmptyCollectionViewCell*)cell;
        emptyCell.delegate = self;
        
    }
    else if ([uniqueIdentifier isEqualToString:PRODUCT_TABLE_CELL]) {
        ProduceDetailCollectionViewCell *produceDetailCollectionViewCell = (ProduceDetailCollectionViewCell*)cell;
        if ([self.delegate respondsToSelector:@selector(updateProductForCell:)]) {
            self.product = [self.delegate updateProductForCell:indexPath];
        }
        
        produceDetailCollectionViewCell.dataSource = self;
        [produceDetailCollectionViewCell refresh];
        
    }
    return cell;
}

- (NSLayoutConstraint *)constraintForBottomView {
    NSLayoutConstraint *resultConstraint = nil;
    for (NSLayoutConstraint *constraint in self.view.constraints) {
        if ([constraint.identifier isEqualToString:@"1234"]) {
            resultConstraint = constraint;
            break;
        }
    }
    return resultConstraint;
}

#pragma mark - UIScrollViewDelegate method
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self scrollViewWillBeginDragging];

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self scrollViewDidEndDecelerating];
}

#pragma mark - ProductDetailTableViewCellDelegate method
- (void)refresh {
    [self.collectionView reloadData];
}

- (void)scrollViewWillBeginDragging {
    
    NSLog(@"scrollViewWillBeginDragging");
    NSLayoutConstraint * constraint3 = [self constraintForBottomView];
    constraint3.constant = -47;
    [self.view setNeedsUpdateConstraints];
    
    if (slideAway) return;
    slideAway = YES;
    [UIView animateWithDuration:0.5f animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        //
        slideAway = NO;
    }];
}

- (void)scrollViewDidEndDecelerating {
    NSLog(@"scrollViewDidEndDecelerating");
    NSLayoutConstraint * constraint3 = [self constraintForBottomView];
    constraint3.constant = 0;
    [self.view setNeedsUpdateConstraints];
    
    if (slideAway) return;
    slideAway = YES;
    [UIView animateWithDuration:0.5f animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        //
        slideAway = NO;
    }];
}


#pragma mark - ProductDetailTableViewCellDataSource method
- (Product*)getData {
    return self.product;
}

/*
- (void)onAddToCart {
    NSLog(@"onAddToCartClicked");
    
    //TODO add to cart
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)onDimiss {
    NSLog(@"onAddToCartClicked");
    [self dismissViewControllerAnimated:YES completion:nil];
}
 */

#pragma mark - EmptyCollectionViewCellDelegate method
- (void)searchComplete:(BOOL)success {
    
    if (success) {
        [[ProductSearchDataManager instance].search incrementPage];
        [self.collectionView reloadData];
    }
    else {
        [Utilities showStandardAlertViewWithMessage:[@"oops" localizedString] fromViewController:self];
    }
    
//    if (onRefresh) {
//        onRefresh = NO;
//        
//        // End the refreshing
//        if (self.refreshControl) {
//            
//            [self.refreshControl endRefreshing];
//        }
//        
//    }
}



- (IBAction)onAddToCart:(id)sender {
    [[PersistentDataManager instance] addProduct:self.product];
    if (self.delegate && [self.delegate respondsToSelector:@selector(exitProductDetail)]) {
        [self.delegate exitProductDetail];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
