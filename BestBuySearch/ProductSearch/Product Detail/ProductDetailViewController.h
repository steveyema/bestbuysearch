//
//  ProductDetailViewController.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-12.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Product;

@interface ProductDetailViewController : UIViewController

@property (nonatomic, strong) Product *product;

@end
