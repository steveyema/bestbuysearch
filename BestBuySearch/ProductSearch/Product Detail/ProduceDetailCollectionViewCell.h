//
//  ProduceDetailCollectionViewCell.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-14.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Product;

@protocol ProductDetailTableViewCellDelegate;
@protocol ProductDetailTableViewCellDataSource;

@interface ProduceDetailCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id<ProductDetailTableViewCellDataSource> dataSource;
@property (nonatomic, weak) id<ProductDetailTableViewCellDelegate> delegate;
- (void)refresh;
//@property (nonatomic, strong) Product *product;

@end
