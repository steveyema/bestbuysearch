//
//  EmptyTableViewCell.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EmptyTableViewCellDelegate <NSObject>

-(void)searchComplete:(BOOL)success;

@end

@interface EmptyTableViewCell : UITableViewCell

@property (nonatomic, weak) id<EmptyTableViewCellDelegate> delegate;

@end
