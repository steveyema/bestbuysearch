//
//  PersistentDataManager.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-17.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Product;

@interface PersistentDataManager : NSObject

@property (nonatomic, readonly) NSDictionary *productsDictionary;

+(id)instance;
- (Product*)getProductBySku:(NSString *)sku;
- (void)addProduct:(Product *)product;
- (void)enumerateProductsUsingBlock:(void(^)(id key, id obj))block;

@end
