//
//  ProductSearchDataManager.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "JSONModel.h"
#import "ProductSearchDataManager.h"
#import "Search.h"
#import "BestBuySearchSDK.h"
#import "SearchCriteria.h"
#import "SearchResult.h"
#import "BestBuySearchConfig.h"

@interface ProductSearchDataManager(){}

@property (nonatomic, strong) Search *search;

@end

@implementation ProductSearchDataManager

- (id)init {
    if (self = [super init]) {
        _search = [[Search alloc] init];
    }
    return self;
}

+ (ProductSearchDataManager*)instance {
    static ProductSearchDataManager *_instance;
    static dispatch_once_t token;
    if (!_instance) {
        dispatch_once(&token, ^{
            _instance = [[ProductSearchDataManager alloc] init];
        });
    }
    return _instance;
}

- (void)executeSearchWithCompletionBlock:(void(^)(BOOL))completion {
    NSLog(@"%@", self.search.criteria);
    
    __weak ProductSearchDataManager *weakself = self;
    
    [[BestBuySearchSDK instance] getEntityWithClassName:CLASS_NAME_SEARCH_RESULT withPath:BEST_BUY_API_SEARCH_PATH withQueryOptions:[self.search.criteria parameterDictionary]  success:^(id object) {
        weakself.search.result = (SearchResult *)object;
        if (completion) completion(YES);
    } failure:^(NSError *error){
        if (completion) completion(NO);
    }];
}

- (void)productDetailBySku:(NSString *)sku withCompletionBloack:(void(^)(id))completion {
    
    NSString *path = [NSString stringWithFormat:BEST_BUY_API_PRODUCT_DETAIL_PATH, sku];
    
    [[BestBuySearchSDK instance] getEntityWithClassName:CLASS_NAME_PRODUCT_DETAIL withPath:path withQueryOptions:nil success:^(id object) {
        if (completion) completion(object);
    } failure:^(NSError *error){
        if (completion) completion(nil);
    }];
    
}

@end
