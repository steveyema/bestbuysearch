//
//  EmptyTableViewCell.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "EmptyTableViewCell.h"
#import "ProductSearchDataManager.h"

@implementation EmptyTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)layoutSubviews {
    [[ProductSearchDataManager instance] executeSearchWithCompletionBlock:^(BOOL successful) {
        //
        if (self.delegate && [self.delegate respondsToSelector:@selector(searchComplete:)]) {
            [self.delegate searchComplete:successful];
        }
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
