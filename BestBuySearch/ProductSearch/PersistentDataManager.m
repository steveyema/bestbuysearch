//
//  PersistentDataManager.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-17.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "PersistentDataManager.h"
#import "Product.h"

@interface PersistentDataManager(){}

@property (nonatomic, strong) NSDictionary *productsDictionary;

@end

@implementation PersistentDataManager

+(id)instance {
    static dispatch_once_t token;
    static PersistentDataManager *_instance;
    if (!_instance) {
        dispatch_once(&token, ^{
            _instance = [[PersistentDataManager alloc] init];
        });
    }
    return _instance;
}

- (NSDictionary *)productsDictionary {
    if (!_productsDictionary) {
        NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:kAppGroup];
        _productsDictionary = (NSDictionary*)[defaults objectForKey:@"best_buy_products"];
    }
    return _productsDictionary;
}

- (void)enumerateProductsUsingBlock:(void(^)(id key, id obj))block {
    [self.productsDictionary enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        NSData *archivedProduct = (NSData*)obj;
        if (archivedProduct) {
            Product *product = (Product*)[NSKeyedUnarchiver unarchiveObjectWithData:archivedProduct];
            if (block) block(key, product);
        }
        else {
            if (block) block(nil, nil);
        }
    }];
}


- (Product*)getProductBySku:(NSString *)sku {
    
    NSData *archivedProduct = (NSData *)[[self productsDictionary] objectForKey:sku];
    if (archivedProduct) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:archivedProduct];
    }
    return nil;
}

- (void)addProduct:(Product *)product {

    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:kAppGroup];
    NSMutableDictionary *products = [[defaults objectForKey:@"best_buy_products"] mutableCopy];
    if (!products) {
        products = [[NSMutableDictionary alloc] initWithCapacity:1];
    }
    
    NSData *archivedProduct = [NSKeyedArchiver archivedDataWithRootObject:product];
    [products setObject:archivedProduct forKey:product.sku];
    [defaults setObject:products forKey:@"best_buy_products"];
    _productsDictionary = nil;
    [defaults synchronize];
}

@end
