//
//  ProductDetail.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-12.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "Product.h"

@interface ProductDetail : Product

//ignore fields that specific to detail model since there is no requirement to render it yet

@end
