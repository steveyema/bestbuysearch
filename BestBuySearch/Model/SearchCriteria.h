//
//  SearchCriteria.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchCriteria : NSObject

@property (nonatomic, strong) NSString *query;
@property (nonatomic, assign) NSInteger page;

@property (nonatomic, readonly) NSDictionary *parameterDictionary;

@end
