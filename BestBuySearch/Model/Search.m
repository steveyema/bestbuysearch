//
//  Search.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "Search.h"
#import "SearchCriteria.h"
#import "SearchResult.h"
#import "NSArray+Utilities.h"

@interface Search(){}

@property (nonatomic, strong) NSArray *products;

@end

@implementation Search

- (id)init {
    if (self = [super init]) {
        _criteria = [[SearchCriteria alloc] init];
        _products = [[NSArray alloc] init];
        _moreResult = YES;
    }
    return self;
}

- (void)reset {
    self.products = [[NSArray alloc] init];
    self.criteria.page = 1;
    self.moreResult = YES;
}

- (void)setResult:(SearchResult *)result {
    _result = result;
    
    self.moreResult = result.currentPage * result.pageSize < result.total;
    
    if (_result.products && _result.products.count > 0) {
        _products = [_products arrayByAddingObjectsFromArray:[result.products mapUsingCompletionBlock:^id(id obj) {
            return [Utilities objectForJsonObject:obj withClassName:CLASS_NAME_PRODUCT];
        }]];
    }
}

- (void)incrementPage {
    self.criteria.page ++;
}

@end
