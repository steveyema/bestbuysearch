//
//  Product.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "JSONModel.h"

@interface Product : JSONModel

@property (nonatomic, strong) NSString *sku;//": "10303905",
@property (nonatomic, strong) NSString *name;//": "Apple iPad Air 2 64GB With Wi-Fi - Gold",
@property (nonatomic, assign) float regularPrice;//": 646.99,
@property (nonatomic, assign) float salePrice;//": 646.99,
@property (nonatomic, strong) NSString *shortDescription;//": "Do everything you've always done on your iPad but on a thinner, lighter device that still feels right in your hand. The iPad Air 2 boasts a faster A8X chip for snappy performance when launching apps or playing games, plus videos look gorgeous on the Retina display. The Touch ID home button lets you secure your tablet with just your fingerprint.",
@property (nonatomic, strong) NSString *productType;//": null,
@property (nonatomic, strong) NSString *thumbnailImage;//": "/multimedia/Products/150x150/103/10303/10303905.jpg",
@property (nonatomic, assign) float customerRating;//": 4.39,
@property (nonatomic, assign) NSInteger customerRatingCount;//": 15,
@property (nonatomic, assign) NSInteger customerReviewCount;//": 13,
@property (nonatomic, strong) NSString *productUrl;//": "/en-CA/product/apple-apple-ipad-air-2-64gb-with-wi-fi-gold-mh182cl-a/10303905.aspx?path=0affec76f29256777f7bdb468aa551c8en02",
@property (nonatomic, assign) BOOL isAdvertised;//": false,
@property (nonatomic, assign) BOOL isClearance;//": false,
@property (nonatomic, assign) BOOL isInStoreOnly;//": false,
@property (nonatomic, assign) BOOL isOnlineOnly;//": false,
@property (nonatomic, assign) BOOL isPreorderable;//": false,
@property (nonatomic, assign) BOOL isVisible;//": true,
@property (nonatomic, assign) BOOL hasPromotion;//": false,
@property (nonatomic, assign) BOOL isFrenchCompliant;//": true,
@property (nonatomic, assign) NSInteger ehf;//": 0,
@property (nonatomic, strong) NSString *currentRegion;//": null,
@property (nonatomic, assign) BOOL hideSavings;//": false,
@property (nonatomic, assign) BOOL isPriceEndsLabel;//": false,
@property (nonatomic, assign) long saleEndDate;//": null,
@property (nonatomic, strong) NSString *priceUnit;//": ""


@end
