//
//  Search.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SearchCriteria;
@class SearchResult;

@interface Search : NSObject

@property (nonatomic, readonly) NSArray *products;

@property (nonatomic, strong) SearchCriteria *criteria;
@property (nonatomic, strong) SearchResult *result;
@property (nonatomic, assign) BOOL moreResult;

- (void)incrementPage;
- (void)reset;

@end
