//
//  SearchCriteria.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "SearchCriteria.h"

@interface SearchCriteria(){}

@property (nonatomic, strong) NSString *lang;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, strong) NSDictionary *baseParamDictionary;

@end

@implementation SearchCriteria

- (id)init {
    if (self = [super init]) {
        _pageSize = 25;
        _page = 1;
        NSLocale *locale = [NSLocale currentLocale];
        NSString *languageCode = [locale objectForKey:NSLocaleLanguageCode];
        _lang = languageCode;
        
    }
    return self;
}

- (NSDictionary*)baseParamDictionary {
    return @{@"page" : @(self.page),
             @"lang" : self.lang,
             @"pageSize" : @(self.pageSize)};
}

- (NSDictionary *)parameterDictionary {
    
    NSMutableDictionary *result = [self.baseParamDictionary mutableCopy];
    
    if (self.query) {

        NSString *trimmedQuery = [self.query stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if (![trimmedQuery isEqualToString:@""]) {
            [result setObject:trimmedQuery forKey:@"query"];
        }
    }
    
    return [result copy];
}

- (void)setPage:(NSInteger)page {
    if (page > 0) {
        _page = page;
    }
}

- (void)setQuery:(NSString *)query {
    NSString *escapedString = [query stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    _query = escapedString;
}

- (NSString*)description {
    return [NSString stringWithFormat:@"query=%@&page=%ld&pageSize=%ld&lang=%@", (self.query? self.query : @""), self.page, self.pageSize, self.lang];
}


@end
