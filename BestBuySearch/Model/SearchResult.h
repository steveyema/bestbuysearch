//
//  Search.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "JSONModel.h"

@class Product;

@interface SearchResult : JSONModel

@property (nonatomic, strong) NSString *Brand;//": "BestBuyCanada",
@property (nonatomic, assign) long currentPage;//": 1,
@property (nonatomic, assign) long total;//": 2253,
@property (nonatomic, assign) long totalPages;//": 46,
@property (nonatomic, assign) NSInteger pageSize;//": 50,
@property (nonatomic, strong) NSArray *products;//": [
@property (nonatomic, strong) NSArray *paths;//": [],
@property (nonatomic, strong) NSArray *facets;//": null,
@property (nonatomic, strong) NSString *LastSearchDate;//": "2015-10-08T01:18:22",
@property (nonatomic, strong) NSArray *relatedQueries;//": null,
@property (nonatomic, strong) NSArray *relatedCategories;//": null,
@property (nonatomic, strong) NSArray *selectedFacets;//": null,
@property (nonatomic, strong) NSArray *resources;//": null,
@property (nonatomic, strong) NSString *redirectUrl;//": null,
@property (nonatomic, strong) NSArray<Product *> *promotions;//": null

@end
