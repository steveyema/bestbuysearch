//
//  BestBuySearchConfig.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-13.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#ifndef BestBuySearchConfig_h
#define BestBuySearchConfig_h

#define STORYBOARD_PRODUCT_SEARCH @"ProductSearch"

#define CLASS_NAME_PRODUCT @"Product"
#define CLASS_NAME_PRODUCT_DETAIL @"ProductDetail"
#define CLASS_NAME_SEARCH_RESULT @"SearchResult"

#define BEST_BUY_API_DEV_URL @"http://www.bestbuy.ca/api/v2/"
#define BEST_BUY_API_QA_URL @"http://www.bestbuy.ca/api/v2/"
#define BEST_BUY_API_PRODUCTION_URL @"http://www.bestbuy.ca/api/v2/"
#define BEST_BUY_IMAGE_BASE_URL @"http://www.bestbuy.ca"

#define BEST_BUY_API_SEARCH_PATH @"json/search"
#define BEST_BUY_API_PRODUCT_DETAIL_PATH @"json/product/%@"

#define kAppGroup @"group.bestbuysearch"

#define NO_IMAGE_NAME @"no-image"

#endif /* BestBuySearchConfig_h */
