//
//  NSString+Localization.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-12.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Localization)

-(NSString*)localizedString;
-(NSString*)localizedUppercaseString;
-(NSString*)localizedCapitalizedString;

@end
