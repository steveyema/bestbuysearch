//
//  NSArray+Utilities.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Utilities)

/**
 *  Map self to a new NSArray
 *
 *  @param completion block that will be executed and regenrate object base on existing one, returns nil if it is not qualified to be added into the result array
 *
 *  @return Mapped array
 */
- (NSArray*)mapUsingCompletionBlock:(id(^)(id))completion;

@end
