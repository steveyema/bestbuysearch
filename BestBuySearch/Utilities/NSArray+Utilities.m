//
//  NSArray+Utilities.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "NSArray+Utilities.h"

@implementation NSArray (Utilities)

- (NSArray*)mapUsingCompletionBlock:(id(^)(id))completion {
    NSMutableArray *resultArray = [[NSMutableArray alloc] initWithCapacity:self.count];
    [self enumerateObjectsUsingBlock:^(id  __nonnull obj, NSUInteger idx, BOOL * __nonnull stop) {
        if (completion) {
            id result = completion(obj);
            if (result) {
                [resultArray addObject:result];
            }
        }
    }];
    return [resultArray copy];
}

@end
