//
//  NSString+Localization.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-12.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "NSString+Localization.h"

#import "NSString+Localization.h"

@implementation NSString (Localization)

-(NSString*)localizedCapitalizedString
{
    return [NSLocalizedString(self, nil) capitalizedString];
}

-(NSString*)localizedString
{
    return NSLocalizedString(self, nil);
}

-(NSString*)localizedUppercaseString
{
    return [NSLocalizedString(self, nil) uppercaseString];
}

@end
