//
//  Utilities.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "Utilities.h"
#import "JSONModel.h"
#import <UIKit/UIKit.h>

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad


@implementation Utilities

+ (id)objectForData:(id)data withClassName:(NSString*)className {

    if ([data isKindOfClass:[NSData class]]) {

        NSError* error = nil;
        id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        if (!error) {
            return [[self class] objectForJsonObject:jsonObject withClassName:className];
        }
    }
    return nil;
}

+ (id)objectForJsonObject:(id)jsonObject withClassName:(NSString*)className {
    if (jsonObject != nil && [jsonObject isKindOfClass:[NSDictionary class]]) {
        NSError *err;
        id object = [[NSClassFromString(className) alloc] initWithDictionary:jsonObject error:&err];
        if (!err) {
            return object;
        }
    }
    return nil;
}

+(void)showStandardAlertViewWithMessage:(NSString*)message fromViewController:(UIViewController*)viewController
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:[@"ok_button" localizedUppercaseString] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:action];
    [viewController presentViewController:alertController animated:YES completion:nil];
}

+ (CGFloat)calculateSizeBaseOnScreenSize:(CGFloat)size
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    CGFloat basewidth = 600.0f;
    return (size / basewidth) * screenWidth;
}

@end
