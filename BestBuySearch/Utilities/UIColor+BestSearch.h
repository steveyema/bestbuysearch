//
//  UIColor+BestSearch.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-12.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (BestSearch)

/**
 *  Utility method to generate UIColor with NSStringified color code
 *
 *  @param hex String value of the color code
 *
 *  @return UIColor object
 */
+(UIColor*)colorWithHexString:(NSString*)hex;

@end
