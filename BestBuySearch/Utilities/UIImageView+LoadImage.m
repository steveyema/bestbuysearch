//
//  UIImageView+LoadImage.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "UIImageView+LoadImage.h"
#import "BestBuySearchSDK.h"
#import "NSString+MD5.h"
#import "FTWCache.h"

@implementation UIImageView (LoadImage)

- (void)loadImageFromURL:(NSURL *)imageUrl withPlaceholder:(NSString*)placeholder withCompletionBlock:(void(^)(UIImage *))completion {

    NSString *key = [imageUrl.absoluteString MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        self.image = image;
    }
    else
    {
        //in case no photo can be found, need to reset imageview to show placeholder image
        if (placeholder) {
            self.image = [UIImage imageNamed:placeholder];
        }
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:imageUrl];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (data) {
                    [FTWCache setObject:data forKey:key];
                    
                    if (completion) completion([UIImage imageWithData:data]);
                }
            });
            
        });
    }
}

@end
