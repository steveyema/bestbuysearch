//
//  Utilities.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utilities : NSObject

+ (id)objectForData:(id)data withClassName:(NSString*)className;
+ (id)objectForJsonObject:(id)jsonObject withClassName:(NSString*)className;
+(void)showStandardAlertViewWithMessage:(NSString*)message fromViewController:(UIViewController*)viewController;
+ (CGFloat)calculateSizeBaseOnScreenSize:(CGFloat)size;

@end
