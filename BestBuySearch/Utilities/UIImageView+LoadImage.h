//
//  UIImageView+LoadImage.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (LoadImage)

/**
 *  Image async load utility
 *
 *  @param imageUrl        Image url
 *  @param completion Block returns the image data
 */
- (void)loadImageFromURL:(NSURL *)imageUrl withPlaceholder:(NSString*)placeholder withCompletionBlock:(void(^)(UIImage *))completion;

@end
