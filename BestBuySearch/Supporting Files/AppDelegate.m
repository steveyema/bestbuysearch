//
//  AppDelegate.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "AppDelegate.h"
#import "ProductSearchDataManager.h"
#import "PersistentDataManager.h"
#import "Product.h"
#import "ProductDetail.h"

#define VIEW_CONTROLLER_PRODUCT_SEARCH @"ProductSearchViewController"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:STORYBOARD_PRODUCT_SEARCH bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:VIEW_CONTROLLER_PRODUCT_SEARCH];
    self.window.rootViewController =viewController;
    
    //register notifications
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge
                                                                                             |UIUserNotificationTypeSound
                                                                                             |UIUserNotificationTypeAlert) categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
        //Do house clean for pre-scheduled local notification
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }
    
    
    //background fetching
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    // Handle launching from a notification
    UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (locationNotification) {
        // Set icon badge number to zero
        application.applicationIconBadgeNumber = 0;
    }
    return YES;
}

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    NSLog(@"performFetchWithCompletionHandler");
    
    [[PersistentDataManager instance] enumerateProductsUsingBlock:^(NSString *key, Product *product) {
        if (product) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                [[ProductSearchDataManager instance] productDetailBySku:product.sku withCompletionBloack:^(id obj) {
                    ProductDetail *productDetail = (ProductDetail *)obj;
                    if (productDetail) {
                        if (productDetail.salePrice < product.salePrice) {
                            NSString *template = product.salePrice == product.regularPrice? @"Price of '%@' reduced from %@ to %@!" : @"Price of '%@' reduced even more from %@ to %@!";
                            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                            localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:2];
                            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
                            [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
                            NSString *originalProductString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:product.salePrice]];
                            NSString *currentProductString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:productDetail.salePrice]];
                            localNotification.alertBody = [NSString stringWithFormat:template, productDetail.name, originalProductString, currentProductString];
                            localNotification.timeZone = [NSTimeZone defaultTimeZone];
                            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                        }
                    }
                }];
            });
        }
    }];
    

    
    completionHandler(UIBackgroundFetchResultNewData);
}

#pragma mark - notifications
-(void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failed to register for push");
}

-(void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
}

-(void) application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reminder"
                                                        message:notification.alertBody
                                                       delegate:self cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    // Request to reload table view data
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
    
    // Set icon badge number to zero
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
