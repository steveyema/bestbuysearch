//
//  NotationImageView.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-18.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "NotationView.h"

@interface NotationView(){}

@property (nonatomic, strong) CAShapeLayer *lineLayer;

@end

@implementation NotationView

- (void)setNotationType:(NOTATION_TYPE)notationType {
    if (_notationType == notationType) return;
    
    _notationType = notationType;
    [self setNeedsDisplay];
}


- (void)drawRect:(CGRect)rect {
    
    [super drawRect:rect];
    
    if (self.lineLayer) {
        [self.lineLayer removeFromSuperlayer];
    }

    if (self.notationType == NOTATION_TYPE_FOLLOW_ME) {
        CGPoint startPoint = CGPointMake(0, 0);
        CGPoint midPoint = CGPointMake(self.bounds.size.width * 0.25, 0);
        CGPoint endPoint = CGPointMake(0, self.bounds.size.height * 0.25);
        
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        [bezierPath moveToPoint:startPoint];
        [bezierPath addLineToPoint:midPoint];
        [bezierPath addLineToPoint:endPoint];
        [bezierPath addLineToPoint:startPoint];
        
        self.lineLayer = [CAShapeLayer layer];
        self.lineLayer.path = [bezierPath CGPath];
        
        self.lineLayer.strokeColor =  [UIColor redColor].CGColor;
        self.lineLayer.lineWidth = 1;
        self.lineLayer.fillColor = [UIColor redColor].CGColor;
        
        [self.layer addSublayer:self.lineLayer];
    }
}

@end
