//
//  NotationImageView.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-18.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    NOTATION_TYPE_NONE,
    NOTATION_TYPE_FOLLOW_ME
} NOTATION_TYPE;

@interface NotationView : UIView

@property (nonatomic, assign) NOTATION_TYPE notationType;

@end
