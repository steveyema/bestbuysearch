//
//  BestBuySearchSDK.m
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "BestBuySearchSDK.h"
#import "JSONModel.h"
#import "Utilities.h"
#import "BestBuySearchConfig.h"

@interface BestBuySearchSDK(){}

@property (nonatomic, strong) NSString *baseApiUrl;
@property (nonatomic, strong) AFHTTPRequestOperationManager *manager;

@end

@implementation BestBuySearchSDK

- (id)init {
    if (self = [super init]) {
        _baseApiUrl = BEST_BUY_API_PRODUCTION_URL;
        _imageBaseUrl = BEST_BUY_IMAGE_BASE_URL;
        _manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:self.baseApiUrl]];
    }
    return self;
}

+ (BestBuySearchSDK*)instance {
    static BestBuySearchSDK *_instance;
    static dispatch_once_t token;
    if (!_instance) {
        dispatch_once(&token, ^{
            _instance = [[BestBuySearchSDK alloc] init];
        });
    }
    return _instance;
}

-(void) getEntityWithClassName:(NSString *)className withPath:(NSString *)path withQueryOptions:(NSDictionary *)queryOptions success:(void (^)(id))success failure:(void (^)(NSError*))failure {
    
    self.manager.responseSerializer = [AFCompoundResponseSerializer serializer];
    
    [self.manager GET:path parameters:queryOptions success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        id result = [Utilities objectForData:responseObject withClassName:className];
        if (result) {
            if (success) success(result);
        }
        else {
            if (failure) failure(nil);
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        if (failure) failure(error);
    }];
}


@end
