//
//  BestBuySearchSDK.h
//  BestBuySearch
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFURLResponseSerialization.h"
#import <AFNetworking.h>

@interface BestBuySearchSDK : NSObject

@property (nonatomic, strong) NSString *imageBaseUrl;

+(BestBuySearchSDK*)instance;

-(void) getEntityWithClassName:(NSString *)className withPath:(NSString *)path withQueryOptions:(NSDictionary *)queryOptions success:(void (^)(id))success failure:(void (^)(NSError*))failure;

@end
