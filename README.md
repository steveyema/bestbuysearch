# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This App is a working application to search for and display products using Best Buy canada rest API 

Version 1.0

### Technical and design choices made and reason behind ###
Best Buy REST API is the search engine behind to provide fast search experience over internet, thus for the App to achieve a better user experience it focused on below areas.

* SDK - BestBuySearchSDK simple and focused on providing the critical networking operation base on AFNetworking to provide a non-block service, it works with dedicated Domain DataManager to provide consistent data service for search screen as well as product detail screen.

* Imaging (cache, canceling) - Concern is loading image for every single produce could be time consuming especially at slower networking, in order to achieve non-blocking and some level of caching ability to reuse already downloaded images, Image is downloaded in background queue and only gets render to UI when it make sense (if the UI component that request the Image has been move out of the visible area after image is downloaded, it would drop the rendering to avoid waste of CPU cycle) Caching library is used to manage reuse image.

* Data Modeling - Product and ProductDetail JSON response is complicate and it's ideal to convert it to Strong typed Data Model, by using third party library, it's done with least effect. Search, SearchCriteria are created to track user search and manage page management (below in detail)

* Paging mechanism - use dedicate UI component to serve as driver for manage pulling next page whenever necessary, in combination with a list  of Model to help manage search, criteria etc.

* Category - Used to provide less coupling function support, such as image async loading/caching, localization, Array mapping etc.

### How do I get set up? ###

Xcode 7.0 with iOS 8.4, 9.0 simulator, Device with iOS 8.4, 9.0, Support iPhone iPad, portrait and landscape mode

* BestBuySearchConfig.h contains user configuration including API URL, Class Name, assets name, view controller and storyboard name etc.

* AFNetworking 2.5.0' -  Networking
JSONModel 1.0.2' - A JSON model utility that is very useful to normalize search result and product detail model
OCMock 3.2' - Mock stub to be used for Unit testing with XCTest

Cocoa pod and submodule is used for dependencies management, please refer to https://cocoapods.org for details, git submodule is a git tool that is used in this App for manage some of the third party reusable library, please refer to https://git-scm.com/docs/git-submodule for how to use it.

* Unit test has been written to cover testing for BestBuySearchSDK where I think make most sense, use XCTest detail please refer to Apple document for details. UI integration testing could be done as well use latest Apple native support given more time.

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Steve Ma
steveyema@gmail.com