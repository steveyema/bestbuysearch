//
//  BestBuySearchTests.m
//  BestBuySearchTests
//
//  Created by Ye Ma on 2015-10-07.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "BestBuySearchSDK.h"
#import "Utilities.h"
#import "SearchResult.h"
#import "BestBuySearchConfig.h"
#import "Product.h"

@interface BestBuySearchTests : XCTestCase

@end

@implementation BestBuySearchTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(id)stubResults {
    NSString *file = [[NSBundle bundleForClass:[self class]] pathForResource:@"searchresults" ofType:@"json"];
    NSData *dta = [NSData dataWithContentsOfFile:file];
    
    return [Utilities objectForData:dta withClassName:@"SearchResult"];
}

- (id)stubResultsProduct {
    return [[Product alloc] init];
}

- (void)testBestBuySearchSuccessful {

    id mock = [OCMockObject mockForClass:[BestBuySearchSDK class]];
    [[[mock stub]andDo:^(NSInvocation *invocation) {
        //
        id searchResult = [self stubResults];
        
        void (^responseHandler)(id) = nil;
        
        [invocation getArgument:&responseHandler atIndex:5];
        
        responseHandler(searchResult);
    }] getEntityWithClassName:[OCMArg any] withPath:[OCMArg any] withQueryOptions:[OCMArg any] success:[OCMArg any] failure:[OCMArg any]];

    [mock getEntityWithClassName:[OCMArg any] withPath:[OCMArg any] withQueryOptions:@{} success:^(id obj) {
        SearchResult* result = (SearchResult *)obj;
        
        XCTAssert(result != nil);
    } failure:^(NSError *error){
        XCTAssertFalse(YES, @"shouldn't has error");
    }];
}

- (void)testBestBuySearchFail {
    //1
    id mock = [OCMockObject mockForClass:[BestBuySearchSDK class]];
    
    //2
    [[[mock stub]andDo:^(NSInvocation *invocation) {
        
        //the block we will invoke
        void (^errorHandler)(NSError *)= nil;
        
        [invocation getArgument:&errorHandler atIndex:6];
        
        //invoke the block
        errorHandler(nil);
    }]  getEntityWithClassName:[OCMArg any] withPath:[OCMArg any] withQueryOptions:[OCMArg any] success:[OCMArg any] failure:[OCMArg any]];
    
    //3
    [mock getEntityWithClassName:[OCMArg any] withPath:[OCMArg any] withQueryOptions:[OCMArg any] success:^(id obj) {
        XCTAssertFalse(YES, @"should not see this");
    } failure:^(NSError *error){
        //
        XCTAssertTrue(YES);
    }];
}

- (void)testBestBuyProductDetailSuccessful {

    id mock = [OCMockObject mockForClass:[BestBuySearchSDK class]];
    
    [[[mock stub]andDo:^(NSInvocation *invocation) {
        id product = [self stubResultsProduct];
        
        void (^responseHandler)(id) = nil;
        
        [invocation getArgument:&responseHandler atIndex:5];
        
        responseHandler(product);
    }] getEntityWithClassName:[OCMArg any] withPath:[OCMArg any] withQueryOptions:[OCMArg any] success:[OCMArg any] failure:[OCMArg any]];
    
    [mock getEntityWithClassName:[OCMArg any] withPath:[OCMArg any] withQueryOptions:[OCMArg any] success:^(id obj) {
        Product *product = (Product *)obj;
        XCTAssert(product != nil);
    } failure:^(NSError *error) {
        XCTAssertFalse(@"shouldn't has error");
    }];
}

- (void)testBestBuyProductDetailFailure {
    
    id mock = [OCMockObject mockForClass:[BestBuySearchSDK class]];
    
    [[[mock stub]andDo:^(NSInvocation *invocation) {
        
        void (^errorHandler)(NSError *) = nil;
        
        [invocation getArgument:&errorHandler atIndex:6];
        
        errorHandler(nil);
        
    }] getEntityWithClassName:[OCMArg any] withPath:[OCMArg any] withQueryOptions:[OCMArg any] success:^(id obj) {
        XCTAssertFalse(YES, @"shouldn't see this");
    } failure:^(NSError *error) {
        XCTAssertTrue(YES);
    }];
}


@end
